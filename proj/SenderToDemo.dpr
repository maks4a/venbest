program SenderToDemo;

//
// Pubsub envelope publisher
// @author Varga Balazs <bb.varga@gmail.com>
//
{$APPTYPE CONSOLE}

uses
  SysUtils,
  zmqapi,
  App.Types in '..\sources\App.Types.pas',
  App.Helpers in '..\sources\App.Helpers.pas';

procedure SendOkMsg(APublisher: TZMQSocket);
var
  LOk: TMsgInput;
begin
  LOk := TMsgInput.Create;
  try
    LOk.&Type := 'login';
    LOk.email := 'foo@bar.baz';
    LOk.Password := 'xxx';
    LOk.MessageID := '1';
    APublisher.send(['api_in', UTF8String(TJsonHelper.ToJSON(LOk))])
  finally
    LOk.Free;
  end;
end;

procedure SendBadMsg(APublisher: TZMQSocket);
var
  LOk: TMsgInput;
begin
  LOk := TMsgInput.Create;
  try
    LOk.&Type := 'login';
    LOk.email := ''; // empty field
    LOk.Password := 'xxx';
    LOk.MessageID := '1';
    APublisher.send(['api_in', UTF8String(TJsonHelper.ToJSON(LOk))])
  finally
    LOk.Free;
  end;
end;

procedure SendBadMsg1(APublisher: TZMQSocket);
var
  LOk: TMsgInput;
begin
  LOk := TMsgInput.Create;
  try
    LOk.&Type := 'login';
    LOk.email := 'foo@bar.baz';
    LOk.Password := 'yyy'; // incorrect password
    LOk.MessageID := '1';
    APublisher.send(['api_in', UTF8String(TJsonHelper.ToJSON(LOk))])
  finally
    LOk.Free;
  end;
end;

procedure Run;
var
  context: TZMQContext;
  publisher: TZMQSocket;
begin
  context := TZMQContext.Create;
  publisher := context.Socket(stPub);
  try
    publisher.bind('tcp://*:20201');
    while true do
    begin
      // Write two messages, each with an envelope and content
      SendOkMsg(publisher);
      SendBadMsg(publisher);
      SendBadMsg1(publisher);
      sleep(1000);
    end;
  finally
    publisher.Free;
    context.Free;
  end;
end;

begin
  try
    { TODO -oUser -cConsole Main : Insert code here }
    ReportMemoryLeaksOnShutdown := true;
    Run;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;

end.
