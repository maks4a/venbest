program zeroMqDemo;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  System.SysUtils,
  App.Core in '..\sources\App.Core.pas',
  App.Types in '..\sources\App.Types.pas',
  App.Helpers in '..\sources\App.Helpers.pas';

procedure Run;
var
  LCore: TAppCore;
begin
  LCore := TAppCore.Create;
  try
    LCore.Connect;
    LCore.Run;
  finally
    LCore.Free;
  end;
end;

begin
  try
    { TODO -oUser -cConsole Main : Insert code here }
    ReportMemoryLeaksOnShutdown := True;
    Run;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;

end.
