﻿unit App.Core;

interface

uses
{$REGION 'https://github.com/bvarga/delphizmq'}
  zmqapi,
{$ENDREGION}
{$REGION 'https://github.com/GabrielF7/ZeosLib'}
  ZConnection,
  ZDataset,
{$ENDREGION}
{$REGION 'System'}
  System.Classes,
  System.Hash,
  System.SysUtils,
{$ENDREGION}
{$REGION 'App'}
  App.Helpers,
  App.Types;
{$ENDREGION}

type
  TAppConfig = class
  private
    FParams: TStringList;
  public
    procedure RegParam(const AValue: string);
    function Read(const AKey: string): string;
    constructor Create;
    destructor Destroy; override;
  end;

  TAppCore = class
  private
    FConfig: TAppConfig;
    FContext: TZMQContext;
    FPublisher: TZMQSocket;
    FSubscriber: TZMQSocket;
    FDbConnect: TZConnection;
    FIsRunned: Boolean;
  protected
    /// <summary>
    /// 1. принять следующие аргументы командной строки
    /// pub, sub, broker-host, dbhost, db-port, db-name, db-user, db-pwd
    /// </summary>
    procedure DoParsingParamStr;
    /// <summary>
    /// 2. подключиться ZeroMQ PUB-сокетом по TCP к хосту broker-host на порт pub
    /// 3. подключиться ZeroMQ SUB-сокетом по TCP к хосту broker-host на порт sub
    /// </summary>
    procedure DoZeroConnect;
    /// <summary>
    /// 4. подключиться к СУБД PostgreSQL с использованием данных, переданных в
    /// аргументах с префиксом db
    /// </summary>
    procedure DoDbConnect;
    /// <summary>
    /// 5. подписаться SUB-сокетом на сообщения с первым фреймом api_in
    /// </summary>
    procedure Subscribe(const AFirstFrame: string);
    /// <summary>
    /// при получении сообщения по SUB-сокету
    /// </summary>
    procedure ParseMsgFromSub(AMsg: TZMQMsg);
    /// <summary>
    /// Подготавливает Запрос для выборки данных из БД
    /// </summary>
    procedure PrepareQuery(AQuery: TZQuery; AInpMsg: TMsgInput);
    /// <summary>
    /// Подготавливает негативный ответ
    /// </summary>
    procedure FillErrMsg(AInput: TMsgInput; var AErrMsg: TMsgOutErr);
    /// <summary>
    /// Подготавливает позитивный ответ
    /// </summary>
    procedure FillOkMsg(AInput: TMsgInput; AQuery: TZQuery; var AOkMsg: TMsgOutOk);
    /// <summary>
    /// Отправка сообщения с указанием кода ошибки
    /// </summary>
    procedure DoSendErrWork(AInput: TMsgInput);
    /// <summary>
    /// Отправка успешного сообщения, все ок
    /// </summary>
    procedure DoSendOkWork(AInput: TMsgInput; AQuery: TZQuery);
    /// <summary>
    /// Работа с БД
    /// </summary>
    function WorkWithDb(AInput: TMsgInput): string;
    /// <summary>
    /// Отправляет сообщение клиенту
    /// </summary>
    procedure SendMsg(const AMsg: string);
  public
    procedure Connect;
    procedure Run;
    procedure Stop;
    constructor Create;
    destructor Destroy; override;
  end;

implementation

{ TAppCore }

procedure TAppCore.Connect;
begin
  DoZeroConnect;
  DoDbConnect;
end;

constructor TAppCore.Create;
begin
  FConfig := TAppConfig.Create;
  DoParsingParamStr;
  FContext := TZMQContext.Create;
  FPublisher := FContext.Socket(stPub);
  FSubscriber := FContext.Socket(stSub);
  FDbConnect := TZConnection.Create(nil);
  FDbConnect.LibraryLocation := ExtractFilePath(ParamStr(0)) + 'libpq.dll';
  Assert(FileExists(FDbConnect.LibraryLocation), 'libpq.dll not found');
  FDbConnect.Protocol := 'postgresql';
end;

destructor TAppCore.Destroy;
begin
  FConfig.Free;
  FDbConnect.Free;
  FPublisher.Free;
  FSubscriber.Free;
  FContext.Free;
  inherited;
end;

procedure TAppCore.DoDbConnect;
begin
  FDbConnect.HostName := FConfig.Read('db-host');
  FDbConnect.Port := FConfig.Read('db-port').ToInteger;
  FDbConnect.Database := FConfig.Read('db-name');
  FDbConnect.User := FConfig.Read('db-user');
  FDbConnect.Password := FConfig.Read('db-pwd');
  FDbConnect.Connect;
end;

procedure TAppCore.DoParsingParamStr;
var
  I: Integer;
begin
  if ParamCount < 8 then
    raise Exception.Create('Not enough parameters');
  for I := 1 to ParamCount do
  begin
    FConfig.RegParam(ParamStr(I));
    Writeln(ParamStr(I));
  end;
end;

procedure TAppCore.DoSendErrWork(AInput: TMsgInput);
var
  LBad: TMsgOutErr;
begin
  Assert(AInput <> nil, 'AInput is nil');
  LBad := TMsgOutErr.Create;
  try
    FillErrMsg(AInput, LBad);
    SendMsg(TJsonHelper.ToJSON(LBad))
  finally
    LBad.Free;
  end;
end;

procedure TAppCore.DoSendOkWork(AInput: TMsgInput; AQuery: TZQuery);
var
  LOk: TMsgOutOk;
begin
  Assert(AInput <> nil, 'AInput is nil');
  Assert(AQuery <> nil, 'AQuery is nil');
  LOk := TMsgOutOk.Create;
  try
    FillOkMsg(AInput, AQuery, LOk);
    SendMsg(TJsonHelper.ToJSON(LOk));
  finally
    LOk.Free;
  end;
end;

procedure TAppCore.DoZeroConnect;
var
  LHost: string;
  LPortSub: Integer;
  LPortPub: Integer;
begin
  LHost := FConfig.Read('broker-host');
  LPortSub := FConfig.Read('sub').ToInteger;
  LPortPub := FConfig.Read('pub').ToInteger;
  FSubscriber.Connect(AnsiString(Format('tcp://%s:%d', [LHost, LPortSub])));
  Subscribe('api_in');
  FPublisher.bind(AnsiString(Format('tcp://%s:%d', [LHost, LPortPub])));
end;

procedure TAppCore.FillErrMsg(AInput: TMsgInput; var AErrMsg: TMsgOutErr);
begin
  Assert(AInput <> nil, 'AInput is nil');
  AErrMsg.MessageID := AInput.MessageID;
  AErrMsg.status := 'error';
  if AInput.IsWrongFormat then
    AErrMsg.error := 'WRONG_FORMAT'
  else
    AErrMsg.error := 'WRONG_PWD';
end;

procedure TAppCore.FillOkMsg(AInput: TMsgInput; AQuery: TZQuery; var AOkMsg: TMsgOutOk);
begin
  Assert(AInput <> nil, 'AInput is nil');
  Assert(AQuery <> nil, 'AQuery is nil');
  AOkMsg.MessageID := AInput.MessageID;
  AOkMsg.status := 'ok';
  AOkMsg.UserID := AQuery.FieldByName('user_id').AsString;
end;

procedure TAppCore.ParseMsgFromSub(AMsg: TZMQMsg);
var
  LJsonFrame: TZMQFrame;
  LInputMsg: TMsgInput;
begin
  Assert(AMsg <> nil, 'AMsg is nil');
  LJsonFrame := AMsg.item[1];
  Assert(LJsonFrame <> nil, 'LJsonFrame is nil');
  LInputMsg := TJsonHelper.FromJSON<TMsgInput>(UTF8ToString(LJsonFrame.asUtf8String));
  try
    if LInputMsg.&Type = 'login' then
      WorkWithDb(LInputMsg);
  finally
    LInputMsg.Free;
  end;
end;

procedure TAppCore.PrepareQuery(AQuery: TZQuery; AInpMsg: TMsgInput);
begin
  Assert(AQuery <> nil, 'AQuery is nil');
  Assert(AInpMsg <> nil, 'AInpMsg is nil');
  AQuery.SQL.Text :=
    'SELECT * FROM public.postgres WHERE ((email = :email) AND (passw = :passw)) LIMIT 1;';
  AQuery.ParamCheck := True;
  AQuery.ParamByName('email').AsString := AInpMsg.email;
  AQuery.ParamByName('passw').AsString := THashMD5.GetHashString(AInpMsg.Password);
  AQuery.Connection := FDbConnect;
end;

procedure TAppCore.Run;
var
  LMsg: TZMQMsg;
begin
  FIsRunned := True;
  while FIsRunned do
  begin
    LMsg := TZMQMsg.Create;
    try
      FSubscriber.recv(LMsg);
      ParseMsgFromSub(LMsg);
      Sleep(500);
    finally
      FreeAndNil(LMsg);
    end;
  end;
end;

procedure TAppCore.SendMsg(const AMsg: string);
begin
  Assert(not AMsg.IsEmpty, 'AMsg is empty');
  FPublisher.send(['api_out', UTF8String(AMsg)]); // Utf8String
  Writeln('send: ' + 'api_out' + ' ' + AMsg);
end;

procedure TAppCore.Stop;
begin
  FIsRunned := False;
end;

procedure TAppCore.Subscribe(const AFirstFrame: string);
begin
  Assert(not AFirstFrame.IsEmpty, 'AFirstFrame is empty');
  FSubscriber.Subscribe(AnsiString(AFirstFrame));
end;

function TAppCore.WorkWithDb(AInput: TMsgInput): string;
var
  LQuery: TZQuery;
begin
  Assert(AInput <> nil, 'AInput is nil');
  if AInput.IsWrongFormat then
  begin
    DoSendErrWork(AInput);
    Exit;
  end;
  LQuery := TZQuery.Create(nil);
  try
    PrepareQuery(LQuery, AInput);
    LQuery.Active := True;
    if LQuery.FieldByName('user_id').IsNull then
      DoSendErrWork(AInput)
    else
      DoSendOkWork(AInput, LQuery);
  finally
    LQuery.Free;
  end;
end;

{ TAppConfig }

constructor TAppConfig.Create;
begin
  FParams := TStringList.Create;
end;

destructor TAppConfig.Destroy;
begin
  FParams.Free;
  inherited;
end;

function TAppConfig.Read(const AKey: string): string;
begin
  Assert(FParams.IndexOfName(AKey) > -1, 'Unknown parameter');
  Result := FParams.Values[AKey];
end;

procedure TAppConfig.RegParam(const AValue: string);
begin
  FParams.Add(AValue);
end;

end.
