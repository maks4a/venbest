﻿unit App.Types;

interface

uses
  DJSON.Attributes;

type
  TMsgBase = class
  public
    [djName('msg_id')]
    MessageID: string;
  end;

  TMsgInput = class(TMsgBase)
  public
    [djName('type')]
    &Type: string;
    [djName('email')]
    Email: string;
    [djName('pwd')]
    Password: string;
    function IsWrongFormat: Boolean;
  end;

  TMsgOut = class(TMsgBase)
    [djName('status')]
    Status: string;
  end;

  // успех
  TMsgOutOk = class(TMsgOut)
  public
    [djName('user_id')]
    UserID: string; // айди пользователя из БД
  end;

  // ошибкa
  TMsgOutErr = class(TMsgOut)
  public
    [djName('error')]
    Error: string;
  end;

implementation

uses
  System.SysUtils;

{ TMsgInput }

function TMsgInput.IsWrongFormat: Boolean;
begin
  Result := Email.IsEmpty or Password.IsEmpty;
end;

end.
