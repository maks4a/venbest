unit App.Helpers;

interface

type
  TJsonHelper = class
    class function ToJSON(AValue: TObject): string;
    class function FromJSON<T>(const AValue: string): T;
  end;

implementation

uses
  DJSON,
  DJSON.Params;

{ TJsonHelper }

class function TJsonHelper.FromJSON<T>(const AValue: string): T;
var
  LParam: IdjParams;
begin
  LParam := dj.DefaultByFields;
  LParam.Engine := TdjEngine.eDelphiDOM;
  Result := dj.FromJSON(AValue, LParam).&To<T>;
end;

class function TJsonHelper.ToJSON(AValue: TObject): string;
var
  LParam: IdjParams;
begin
  LParam := dj.DefaultByFields;
  LParam.Engine := TdjEngine.eDelphiDOM;
  Result := dj.From(AValue, LParam).ToJSON;
end;

end.
